#!/bin/bash

set -eu

echo "==> Creating directories"
mkdir -p /run/cryptpad /app/data/blob /app/data/datastore /app/data/block /app/data/customize /app/data/data /run/nginx /run/npm

if [[ ! -f /app/data/config.js ]]; then
    echo "==> Creating config on first run"
    cp /app/pkg/config.js /app/data/config.js
fi

sed -e "s/api.your-main-domain.com/${CLOUDRON_APP_DOMAIN}/" \
    -e "s/files.your-main-domain.com/${CLOUDRON_APP_DOMAIN}/" \
    -e "s/your-main-domain.com/${CLOUDRON_APP_DOMAIN}/" \
    -e "s/your-sandbox-domain.com/${SANDBOX_DOMAIN}/" \
    /app/pkg/nginx.conf > /run/nginx/cryptpad

chown -R cloudron:cloudron /app/data /run/cryptpad /run/npm

echo "==> Building CryptPad"
gosu cloudron:cloudron npm run build

echo "==> Starting CryptPad"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i CryptPad
