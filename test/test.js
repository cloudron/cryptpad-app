#!/usr/bin/env node

/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 30000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

    let browser, app;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function clearCache() {
        await browser.manage().deleteAllCookies();
        await browser.quit();
        browser = null;
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        chromeOptions.addArguments(`--user-data-dir=${await fs.promises.mkdtemp('/tmp/test-')}`); // --profile-directory=Default
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
    }

    async function exists(selector) {
        await browser.wait(until.elementLocated(selector), TEST_TIMEOUT);
    }

    async function visible(selector) {
        await exists(selector);
        await browser.wait(until.elementIsVisible(browser.findElement(selector)), TEST_TIMEOUT);
    }

    async function signup() {
        await browser.get(`https://${app.fqdn}/register`);
        await browser.sleep(3000);
        await visible(By.id('username'));
        await browser.findElement(By.id('username')).sendKeys('admin');
        await browser.findElement(By.id('password')).sendKeys('supersecret');
        await browser.findElement(By.id('password-confirm')).sendKeys('supersecret');
        // await exists(By.id('accept-terms'));
        // const checkbox = browser.findElement(By.id('accept-terms'));
        // await browser.executeScript('arguments[0].checked=true', checkbox);
        await browser.findElement(By.id('register')).click();
        await visible(By.xpath('//button[contains(text(), "I have written down")]'));
        await browser.findElement(By.xpath('//button[contains(text(), "I have written down")]')).click();
        await visible(By.id('sbox-iframe'));
        browser.switchTo().frame(browser.findElement(By.id('sbox-iframe')));
        await visible(By.xpath('//button[@aria-label="User menu"]'));
    }

    async function checkup() {
        await browser.get(`https://${app.fqdn}/checkup`);
        await visible(By.xpath('//p[contains(text(), "51 / 55 tests passed")]'));
    }

    async function login() {
        await browser.get(`https://${app.fqdn}/login/`);
        await visible(By.id('name'));
        await browser.findElement(By.id('name')).sendKeys('admin');
        await browser.findElement(By.id('password')).sendKeys('supersecret');
        await browser.findElement(By.xpath('//button[text()="Log in"]')).click();

        await visible(By.id('sbox-iframe'));
        browser.switchTo().frame(browser.findElement(By.id('sbox-iframe')));
        await visible(By.xpath('//button[@aria-label="User menu"]'));
    }

    async function loggedIn() {
        await browser.get(`https://${app.fqdn}/drive/#`);
        await visible(By.id('sbox-iframe'));
        browser.switchTo().frame(browser.findElement(By.id('sbox-iframe')));
        await visible(By.xpath('//button[@aria-label="User menu"]'));
    }

    // async function logout() {
    //     await browser.get(`https://${app.fqdn}/drive/#`);
    //     await visible(By.xpath('//button[@alt="User menu"]'));
    //     await browser.findElement(By.xpath('//button[@alt="User menu"]')).click();

    //     await visible(By.xpath('//a[contains(@class,"fa-sign-out")]'));
    //     await browser.findElement(By.xpath('//a[contains(@class,"fa-sign-out")]')).click();

    //     await visible(By.xpath('//h2[text()="Private by design"]'));
    // }

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION || a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync('cloudron install --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can checkup', checkup);
    it('can sign up', signup);

    it('restart app', function () {
        execSync('cloudron restart --app ' + app.id, EXEC_ARGS);
    });
    it('can checkup', checkup);
    it('is logged in', loggedIn);

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, EXEC_ARGS);
    });

    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw'));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can checkup', checkup);
    it('is logged in', loggedIn);

    it('move to different location', function () {
        browser.manage().deleteAllCookies();
        execSync('cloudron configure --location ' + LOCATION + '2 --app ' + app.id, EXEC_ARGS);
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    });

    it('can checkup', checkup);
    it('can login', login);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    it('can clear cache', clearCache);

    // test update
    it('can install app for update', async function () { execSync(`cloudron install --appstore-id fr.cryptpad.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can sign up', signup);

    it('can update', function () {
        execSync('cloudron update --app ' + app.id, EXEC_ARGS);
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        expect(app).to.be.an('object');
    });

    it('can checkup', checkup);
    it('is logged in', loggedIn);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });
});
