FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code
WORKDIR /app/code

# actual releases have sometimes strange names, but tags are the same
# renovate: datasource=github-tags depName=xwiki-labs/cryptpad versioning=loose extractVersion=^(?<version>.+)$
ARG CRYPTPAD_VERSION=2024.12.0

RUN curl -L https://github.com/xwiki-labs/cryptpad/archive/refs/tags/${CRYPTPAD_VERSION}.tar.gz | tar zvxf - --strip-components 1 && \
    npm install --production && \
    npm run install:components && \
    ./install-onlyoffice.sh --accept-license && \
    npm cache clean --force && \
    chown -R cloudron:cloudron /app/code

COPY nginx_readonlyrootfs.conf /etc/nginx/conf.d/readonlyrootfs.conf
RUN rm /etc/nginx/sites-enabled/* && \
    ln -sf /dev/stdout /var/log/nginx/access.log && \
    ln -sf /dev/stderr /var/log/nginx/error.log && \
    ln -s /run/nginx/cryptpad /etc/nginx/sites-enabled/cryptpad

RUN ln -s /app/data/config.js /app/code/config/config.js && \
    ln -s /app/data/blob /app/code/blob && \
    ln -s /app/data/datastore /app/code/datastore && \
    ln -s /app/data/block /app/code/block && \
    ln -s /app/data/data /app/code/data

RUN ln -s /app/data/customize /app/code/customize

COPY supervisor/ /etc/supervisor/conf.d/
RUN sed -e 's,^logfile=.*$,logfile=/run/cryptpad/supervisord.log,' -i /etc/supervisor/supervisord.conf

COPY nginx.conf config.js start.sh /app/pkg/

RUN rm -rf /home/cloudron/.npm && ln -s /run/npm /home/cloudron/.npm

CMD [ "/app/pkg/start.sh" ]
